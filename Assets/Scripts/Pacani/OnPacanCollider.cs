﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPacanCollider : MonoBehaviour
{
    LadderManager ladderManager;
    void Start()
    {
        ladderManager = FindObjectOfType<LadderManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.transform.tag == "obstacle")
        {
            GameObject falsePacan = GetComponentInParent<Pacan>().gameObject;
            GameObject truePacan = Instantiate(falsePacan, falsePacan.transform.parent);

            Destroy(falsePacan.GetComponent<Pacan>().joints[0]);
            Destroy(falsePacan.GetComponent<Pacan>().joints[1]);

            falsePacan.GetComponentInChildren<BoxCollider>().enabled = false;
            truePacan.GetComponentInChildren<BoxCollider>().enabled = false;

            truePacan.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;

            for(int i = 0; ladderManager.steps.Count - 1 >= i; i++)
            {
                if(ladderManager.steps[i].id == truePacan.GetComponent<Pacan>().id)
                {
                    ladderManager.steps[i].isTaken = false;
                    ladderManager.steps[i].pacan = truePacan;
                }
            }
            //GetComponent<Rigidbody>().AddForce(Vector3.back * 5);
        }
    }
}
