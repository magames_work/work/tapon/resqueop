﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacan : MonoBehaviour
{
    LadderManager ladderManager;
    public int id;
    public FixedJoint[] joints;

    // Start is called before the first frame update
    void Start()
    {
        ladderManager = FindObjectOfType<LadderManager>();

        joints = GetComponentsInChildren<FixedJoint>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
