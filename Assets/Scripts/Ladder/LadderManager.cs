﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LadderManager : MonoBehaviour
{
    public List<Steps> steps = new List<Steps>();

    [Serializable]
    public class Steps
    {
        public int id;
        public bool isTaken;
        public GameObject step, pacan;
    }

    int switcher;

    private void Start() {
        switcher = 0;
        int i = 0;
        foreach(Steps step in steps)
        {
            foreach(FixedJoint joint in step.pacan.GetComponent<Pacan>().joints)
            {
                joint.connectedBody = steps[i].step.GetComponent<Rigidbody>();
            }
            step.pacan.GetComponent<Pacan>().id = i;

            step.id = i;
            i ++;
        }
    }

    bool follow;
    private void Update() 
    {
        
    }

    public void Pickup(Transform pacan)
    {
        //pacan.GetComponentInChildren<BoxCollider>().enabled = true;
        //pacan.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
        int step = MinFreeStep();

        pacan.GetComponent<Animator>().SetTrigger("jump");
        //pacan.transform.DOMove(steps[step].step.transform.position, 1);
        steps[step].isTaken = true;
        
        StartCoroutine(movePacan(pacan.gameObject, steps[step].step, steps[step].pacan));
    }

    IEnumerator movePacan(GameObject falsePacan, GameObject step, GameObject truePacan)
    {
        float catchTime = Time.time;

        falsePacan.transform.Rotate(new Vector3(0, 180, 0));
        while(Time.time < catchTime + 1.6f)
        {
            falsePacan.transform.position = Vector3.MoveTowards(falsePacan.transform.position, step.transform.position - Vector3.up * 1.6f, 0.1f);
            yield return new WaitForEndOfFrame();
        }
        truePacan.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
        falsePacan.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;

    }

    int MinFreeStep()
    {
        List<int> places = new List<int>();
        for(int i = 0; steps.Count - 1 >= i; i++)
        {
            if(!steps[i].isTaken)
                places.Add(steps[i].id);
        }

        int min = 10;
        for(int i = 0; places.Count - 1 >= i; i++)
        {
            if(places[i] < min)
            {
                min = places[i];
            }
        }
        
        return min;
    }
}
