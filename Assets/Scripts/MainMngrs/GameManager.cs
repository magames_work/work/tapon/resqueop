﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private UIManager ui;
    private LvlManager lvl;

    public int health;
    public GameObject heli;

    void Start()
    {
        ui = GetComponent<UIManager>();
        lvl = GetComponent<LvlManager>();
        Application.targetFrameRate = 60;
        Time.timeScale = 1;
    }

    void Update()
    {


        if(health <= 0)
        {
            print("game over");
        }
    }
}
