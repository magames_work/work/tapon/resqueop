﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    private LvlManager lvl;
    private GameManager gm;

    public Slider progresBar;
    public GameObject h1, h2, h3, wrong, lose;

    void Start()
    {
        lvl = GetComponent<LvlManager>();
        gm = GetComponent<GameManager>();
    }

    private void Update()
    {
        HeartUpdate();

        ProgressUpdate();
    }

    private void ProgressUpdate()
    {
        progresBar.maxValue = lvl.lvls[lvl.currentLvl].pointsNeed;
        progresBar.value = lvl.currentPoints;
    }

    private void HeartUpdate()
    {
        if (gm.health == 3)
        {
            h1.SetActive(true);
            h2.SetActive(true);
            h3.SetActive(true);
        }
        else if (gm.health == 2)
        {
            h1.SetActive(true);
            h2.SetActive(true);
            h3.SetActive(false);
        }
        else if (gm.health == 1)
        {
            h1.SetActive(true);
            h2.SetActive(false);
            h3.SetActive(false);
        }
        else if (gm.health == 0)
        {
            h1.SetActive(false);
            h2.SetActive(false);
            h3.SetActive(false);
        }
    }

    public void resetlvl()
    {
        SceneManager.LoadScene(0);
    }


}
