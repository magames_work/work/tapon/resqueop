﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LvlManager : MonoBehaviour
{
    private UIManager ui;
    private GameManager gm;

    public int currentLvl;
    public int currentPoints;

    public List<Lvls> lvls = new List<Lvls>();
    [Serializable]
    public class Lvls
    {
        public GameObject lvl;
        public int pointsNeed;
    }



    void Start()
    {
        ui = GetComponent<UIManager>();
        gm = GetComponent<GameManager>();        
    }

    // Update is called once per frame
    void Update()
    {
        if(currentPoints >= lvls[currentLvl].pointsNeed)
        {
            print("win");
        }
    }
}
