﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fps : MonoBehaviour
{
    public Text uiFps;
    public string formatedString = "{value} FPS";

    public float updateRateSeconds = 4.0F;

    int frameCount = 0;
    float dt = 0.0F;
    float Fps = 0.0F;
    
    private void Start() {
        Application.targetFrameRate = 60;
    }
     void Update()
     {
         frameCount++;
         dt += Time.unscaledDeltaTime;
         if (dt > 1.0 / updateRateSeconds)
         {
             Fps = frameCount / dt;
             frameCount = 0;
             dt -= 1.0F / updateRateSeconds;
         }
         uiFps.text = formatedString.Replace("{value}", System.Math.Round(Fps, 1).ToString("0.0"));
     }
}
