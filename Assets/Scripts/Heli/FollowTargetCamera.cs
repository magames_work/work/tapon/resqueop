using UnityEngine;

public class FollowTargetCamera : MonoBehaviour
{
    public Transform Target;
    public float PositionFolowForce = 5f;
    public float RotationFolowForce = 5f;
    public Vector3 offsetPos;
	void Start ()
	{

	}

    void FixedUpdate()
	{
        // var vector = Vector3.forward;
        // var dir = Target.rotation * Vector3.forward;
		// dir.y = 0f;
        // if (dir.magnitude > 0f) vector = dir / dir.magnitude;


        Vector3 pos = Vector3.Lerp(transform.position, Target.position + offsetPos, PositionFolowForce * Time.deltaTime);

        if(pos.z > transform.position.z)
            transform.position = pos;
        else
            transform.position = new Vector3(pos.x, pos.y, transform.position.z);
        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(vector), RotationFolowForce * Time.deltaTime);
	}
}

