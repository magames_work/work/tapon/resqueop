﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HeliMovement : MonoBehaviour
{
    public float speedFwd, speedSides;
    public float staticSpeed;
    public CustomSwipes swipes;
    public GameObject followcam;


    private GameObject heli;
    void Start()
    {
        heli = gameObject;
    }

    float x;
    Vector2 CatchScreenPos;
    void FixedUpdate()
    {

        if(Input.GetMouseButtonDown(0))
        {
            CatchScreenPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }
        if(Input.GetMouseButton(0))
        {
            // Vector2 screen = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            
            // float x = Mathf.Clamp(6.5f * screen.x - 2.5f, -4, 2.5f);


            Vector2 screen = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            Vector2 dif = screen - CatchScreenPos;

            //dif = dif * speedSides * Time.deltaTime;

            // if(heli.transform.position.x + dif.x < 7f & heli.transform.position.x + dif.x > -7f & heli.transform.position.z - 2 > followcam.transform.position.z)
            //     heli.transform.Translate(new Vector3(dif.x * speedSides * Time.deltaTime, 0 ,dif.y * speedFwd * Time.deltaTime));

            if(heli.transform.position.x + dif.x < 7f)
                heli.transform.Translate(new Vector3(Mathf.Clamp(dif.x, 0, 1) * speedSides * Time.deltaTime, 0, 0));
            if(heli.transform.position.x + dif.x > -7f)
                heli.transform.Translate(new Vector3(Mathf.Clamp(dif.x, -1, 0) * speedSides * Time.deltaTime, 0, 0));
            if(heli.transform.position.z - 2 > followcam.transform.position.z)
                heli.transform.Translate(new Vector3(0, 0, dif.y * speedFwd * Time.deltaTime));
            else
                {
                    if(!Input.GetMouseButton(0))
                        {
                            heli.transform.position = new Vector3(heli.transform.position.x, heli.transform.position.y, followcam.transform.position.z);
                        }
                    heli.transform.Translate(new Vector3(0, 0, Mathf.Clamp(dif.y, 0, 1) * speedFwd * Time.deltaTime));
                }
            
        }
        


        //if(swipes.isSwipeLeft)
        //{
        //    heli.transform.position -= heli.transform.right * speedSides * Time.deltaTime;
        //}

        //if (swipes.isSwipeRight)
        //{
        //    heli.transform.position += heli.transform.right * speedSides * Time.deltaTime;
        //}
    }
}
