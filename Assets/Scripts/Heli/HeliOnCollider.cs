﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeliOnCollider : MonoBehaviour
{
    public GameObject megumin;
    UIManager ui;
    // Start is called before the first frame update
    void Start()
    {
        ui = FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.tag == "obstacle")
        {
            Instantiate(megumin, transform);
            print("game over");
            ui.lose.SetActive(true);
            Time.timeScale = 0.1f;
        }
    }
}
